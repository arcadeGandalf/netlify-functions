const faunadb = require('faunadb'),
  q = faunadb.query;

let client;

exports.handler = async function (event) {
  try {
    client = new faunadb.Client({
      secret: process.env.FAUNADB_SECRET,
      domain: 'db.eu.fauna.com',
      keepAlive: false
    });

    if (event.httpMethod === 'POST') {
      await addRecord();
    }

    if (event.httpMethod === 'DELETE') {
      await removeRecord();
    }

    if (event.httpMethod === 'PUT') {
      await updateRecord();
    }

    const sessions = await client.query(
      q.Map(
        q.Paginate(q.Documents(q.Collection('session'))),
        q.Lambda(x => q.Get(x))
      )
    );

    return {
      statusCode: 200,
      body: JSON.stringify(sessions.data.map(item => item.data))
    };
  } catch (e) {
    console.log(e);

    return {
      statusCode: 500,
      body: JSON.stringify(e)
    };
  }
};

async function addRecord() {
  await client.query(
    q.Create(q.Collection('session'), {
      data: { name: `New ${Date.now()}`, updated: false }
    })
  );
}

async function removeRecord() {
  const document = await client.query(
    q.Map(
      q.Paginate(q.Documents(q.Collection('session')), { size: 1 }),
      q.Lambda(x => q.Get(x))
    )
  );

  await client.query(
    q.Delete(q.Ref(q.Collection('session'), document.data[0].ref.id))
  );
}

async function updateRecord() {
  const documentNotUpdated = await client.query(
    q.Map(
      q.Paginate(q.Match(q.Index('session_updated'), false), { size: 1 }),
      q.Lambda(x => q.Get(x))
    )
  );

  if (!documentNotUpdated) {
    return;
  }

  await client.query(
    q.Update(
      q.Ref(q.Collection('session'), documentNotUpdated.data[0].ref.id),
      {
        data: { name: `New ${Date.now()}`, updated: true }
      }
    )
  );
}
